import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
        int moveCount = 0;
        int currentPlayer = 0;
        while(moveCount < 9){
        printBoard(board);
        int row, col;
        do{      
		System.out.print("Player" + (currentPlayer + 1) + " enter row number");
		row = reader.nextInt();
		System.out.print("Player" + (currentPlayer + 1) + " enter column number");
		col = reader.nextInt();
        } while(!(row > 0 && row < 4 && col > 0 && col < 4 && board[row - 1][col - 1] == ' '));
        board[row - 1][col - 1] = (currentPlayer == 0 ? 'X' : 'O');
        moveCount++;
        if(checkBoard(board, row, col)){
            System.out.println("Player " + (currentPlayer + 1) + " won!");
            break;
        }
        currentPlayer = (currentPlayer + 1) % 2;
        }

		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

    public static boolean checkBoard(char[][] board, int rowLast, int colLast) {
        char symbol = board[rowLast - 1][colLast - 1];
        boolean win = true;
		for (int col = 0; col < 3; col++){
            if(board[rowLast - 1][col] != symbol){
                win = false;
                break;
            }
        }
        if(win)
            return true;
        win = true;
        for (int row = 0; row < 3; row++){
            if(board[row][colLast -1] != symbol){
                win = false;
                break;
            }
        }
        if(win)
            return true;
        win = true;
        for (int row = 0; row < 3; row++){
            if(board[row][row] != symbol){
                win = false;
                break;
            }
        }
        if(win)
            return true;
        return false;

	}

}
