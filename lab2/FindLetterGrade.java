public class FindLetterGrade {

	public static void main(String[] args){
		int grade = Integer.parseInt(args[0]);
        char letter;

        if(grade >= 90){
            letter = 'A';
        }
        else if(grade >= 80){
            letter = 'B';
        }
        else if(grade >= 70){
            letter = 'C';
        }
        else if(grade >= 60){
            letter = 'D';
        }
        else {
            letter = 'F';
        }

        System.out.println(letter);

	}
}
