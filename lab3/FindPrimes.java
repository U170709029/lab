public class FindPrime{
	public static void main(String[] args){
		int x = Integer.parseInt(args[0]);
		int a;
		int b;
		for(int i=2; i < x; i++){
			a = 0;
			b = 2;
			while(a < 2 && b < i){
				if(i % b == 0)
					a++;
				b++;
			}
			if(a == 0){
				System.out.print(i + "  ");
			}
		}
	}
}