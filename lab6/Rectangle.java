package rectangle;
import java.awt.Point;
public class Rectangle{
	int sideA,sideB;
	Point topLeft;
	public Rectangle(Point p, int a, int b){
		this.topLeft = p;
		this.sideA = a;
		this.sideB = b;
	}
	public int area() {
		return this.sideA * this.sideB;
	}
	public int perimeter() {
		return (this.sideA + this.sideB) * 2;
	}
	public Point[] corners() {
		Point topRight = new Point(topLeft.x + this.sideA, topLeft.y);
		Point bottomLeft = new Point(topLeft.x, topLeft.y - this.sideB);
		Point bottomRight = new Point(bottomLeft.x + this.sideA, bottomLeft.y);
		Point[] parr = new Point[4];
		parr[0] = this.topLeft;
		parr[1] = topRight;
		parr[2] = bottomLeft;
		parr[3] = bottomRight;
		return parr;
		}
	
}